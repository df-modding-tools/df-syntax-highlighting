# (DEPRECATED) Dwarf Fortress RAW Syntax Highlighter

**This repo is deprecated. The content of this repo has been merged with https://gitlab.com/df-modding-tools/df-raw-language-server**

This is a syntax highlighter for Dwarf Fortress RAW files.

## Features

Syntax highlighting and highlighting known correct tags with correct arguments.
For the known tags it checks the type and amount of arguments.
It does not yet check the location of the tag.

![Syntax highlighter](images/Basic1.png)
![Syntax highlighter Default VS Code theme](images/Basic2.png)
![Token examples](images/Tokens.png)

## Syntax

If you want to take a look at the syntax this extension is based on look here:
https://gitlab.com/df-modding-tools/df-raw-syntax

## Known Issues

This extension does not yet recognize all the different tags.

## Manually install the extension

### Linux
To start using this extension with VS Code, copy it into the 
`<user home>/.vscode/extensions` folder and restart Code.

## Release Notes

### 0.1.0

Initial release with some basic known tags.

## Contribute

If you want to contribute, join our [Discord](https://discord.gg/6eKf5ZY).

## License

This project is licensed under the [MIT license](https://choosealicense.com/licenses/mit/).

All contributions to this project will be similarly licensed.
